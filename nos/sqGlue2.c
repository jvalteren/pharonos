#include <sq.h>
#include "ints.h"

#define fopen64 fopen
#define MicrosecondsFrom1901To1970 2177452800000000LL
#define DateHack2016 1461715200000000LL

extern Computer computer;

extern unsigned long timer;
/* NO PROFILE */
void 
ioNewProfileStatus(sqInt *running, long *buffersize)
{
	if (running)
		*running = 0;
	if (buffersize)
		*buffersize = 0;
}

long
ioNewProfileSamplesInto(void *sampleBuffer)
{
	return 0;
}

long
ioControlNewProfile(int on, unsigned long buffer_size)
{
	return 0;
}

void
ioClearProfile(void)
{
}

/* -- */

sqInt
amInVMThread() { return 1; }

/* this for testing crash dumps */
static sqInt
indirect(long p)
{
	if ((p & 2))
		error("crashInThisOrAnotherThread");
	return p > 99
		? indirect(p - 100), indirect(p - 50) /* evade tail recursion opt */
		: *(sqInt *)p;
}

/* bit 0 = thread to crash in; 1 => this thread
 * bit 1 = crash method; 0 => indirect through null pointer; 1 => call exit
 */
sqInt
crashInThisOrAnotherThread(sqInt flags)
{
	if ((flags & 1)) {
		if (!(flags & 2))
			return indirect(flags & ~1);
		error("crashInThisOrAnotherThread");
		return 0;
	}
	
	return 0;
}

/* -- */

sqInt ioExitWithErrorCode(int ec){ // FIXME
  return ec;
}

sqInt ioGetWindowWidth()
{ return computer.videoInfo.width; } 

sqInt ioGetWindowHeight()
{ return computer.videoInfo.height; }

char* ioGetWindowLabel(void) {return "";}

sqInt ioSetWindowLabelOfSize(void* lbl, sqInt size)
{ return 1; }

sqInt ioSetWindowWidthHeight(sqInt w, sqInt h) { return 1; }

sqInt ioIsWindowObscured(void) {return false;}

char* ioGetLogDirectory(void) { return ""; };

sqInt ioSetLogDirectoryOfSize(void* lblIndex, sqInt sz){ return 1; }

/* -- */

unsigned volatile long long ioUTCMicroseconds() { return MicrosecondsFrom1901To1970 + DateHack2016 + ioHighResClock() / 1000; } // FIXME

//usqLong ioLocalMicroseconds() { return ioHighResClock(); } // FIXME

unsigned volatile long long ioUTCMicrosecondsNow() { return MicrosecondsFrom1901To1970 + DateHack2016 + ioHighResClock() / 1000; } // FIXME // Time microsecondClockValue

unsigned volatile long long ioLocalMicrosecondsNow() { return MicrosecondsFrom1901To1970 + DateHack2016 + ioHighResClock() / 1000; }; // FIXME

unsigned long long ioUTCStartMicroseconds() { return 0; }

sqInt ioLocalSecondsOffset() { return 10000000 / 1000000LL; }

int
ioSecondsNow(void) { return ioMSecs() / 1000; }

sqLong
ioHighResClock(void)
{
  /* return the value of the high performance counter */
  sqLong value = 0;
#if defined(__GNUC__) && ( defined(i386) || defined(__i386) || defined(__i386__)  \
			|| defined(i486) || defined(__i486) || defined (__i486__) \
			|| defined(intel) || defined(x86) || defined(i86pc) )
    __asm__ __volatile__ ("rdtsc" : "=A"(value));
#elif defined(__arm__) && defined(__ARM_ARCH_6__)
	/* tpr - do nothing for now; needs input from eliot to decide further */
#else
#    error "no high res clock defined"
#endif
  return value;
}

void
ioGetClockLogSizeUsecsIdxMsecsIdx(sqInt *np, void **usecsp, sqInt *uip, void **msecsp, sqInt *mip)
{
	*np = *uip = *mip = 0;
	*usecsp = *msecsp = 0;
}

void
ioUpdateVMTimezone()
{
}

/* -- */

unsigned long
ioHeartbeatFrequency(int resetStats)
{
	return 1000;
}

int ioHeartbeatMilliseconds() { return 2; } // FIXME

void ioSetHeartbeatMilliseconds(int ms) // FIXME
{
}

/* -- */
/* Following two are left for compatibility with language side */
#if 0
void ioSetMaxExtSemTableSize(int n) { 
    /* just ignore */
}

int ioGetMaxExtSemTableSize(void) { 
    // answer an arbitrary large number.. to not confuse image side about it
    return 10000000;
    /*    return maxQueueSize(); */
}
#endif
/* -- */
