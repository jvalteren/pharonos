#include "SerialPlugin.h"

int serialPortInit(void) {
  return 0;
}
int serialPortShutdown(void) {
  return 0;
}
int serialPortClose(int portNum) {
  return 0;
}
int serialPortCount(void) {
  return 1;
}
int serialPortOpen(int portNum, int baudRate, int stopBitsType, int parityType, int dataBits,
		   int inFlowCtrl, int outFlowCtrl, int xOnChar, int xOffChar) {
  return 0;
}
int serialPortOpenByName(char *portName, int baudRate, int stopBitsType, int parityType, int dataBits,
                   int inFlowCtrl, int outFlowCtrl, int xOnChar, int xOffChar) {
  return 0;
}
int serialPortReadInto(int portNum, int count, void *bufferPtr) {
  return 0;
}
int serialPortReadIntoByName(const char *portName, int count, void *bufferPtr) {
  return 0;
}
int serialPortWriteFrom(int portNum, int count, void *bufferPtr){
  int n = 0;
  write_serial_string("serialPortWriteFrom\n");
  char *buff = (char *)bufferPtr;
  for (n = 0; n < count; n++) {
    write_serial(*(buff + n));
  }
  return count;
}
int serialPortWriteFromByName(const char *portName, int count, void *bufferPtr) {
  int n = 0;
  write_serial_string("serialPortWriteFromByName\n");
  char *buff = (char *)bufferPtr;
  for (n = 0; n < count; n++) {
    write_serial(*(buff + n));
  }
  return count;
}